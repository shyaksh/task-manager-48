package ru.bokhan.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.entity.Session;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_session")
public final class SessionDTO extends AbstractEntityDTO implements Cloneable {

    @Column
    @Nullable
    private Long timestamp;

    @Nullable
    @Column(name = "user_id")
    private String userId;

    @Column
    @Nullable
    private String signature;

    public SessionDTO(@NotNull final Session session) {
        this.timestamp = session.getTimestamp();
        this.signature = session.getSignature();
        this.userId = session.getUser().getId();
        this.setId(session.getId());
    }

    @Nullable
    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (@NotNull final CloneNotSupportedException e) {
            return null;
        }
    }

    @Nullable
    public static SessionDTO toDTO(@Nullable final Session session) {
        if (session == null) return null;
        return new SessionDTO(session);
    }

}
