# DefaultApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**count**](DefaultApi.md#count) | **GET** /api/projects/count | 
[**count_0**](DefaultApi.md#count_0) | **GET** /api/tasks/count | 
[**deleteAll**](DefaultApi.md#deleteAll) | **DELETE** /api/projects/all | 
[**deleteAll_0**](DefaultApi.md#deleteAll_0) | **POST** /api/projects/delete | 
[**deleteAll_1**](DefaultApi.md#deleteAll_1) | **DELETE** /api/tasks/all | 
[**deleteAll_2**](DefaultApi.md#deleteAll_2) | **POST** /api/tasks/delete | 
[**deleteById**](DefaultApi.md#deleteById) | **DELETE** /api/project/{id} | 
[**deleteById_0**](DefaultApi.md#deleteById_0) | **DELETE** /api/task/{id} | 
[**existsById**](DefaultApi.md#existsById) | **GET** /api/project/exists/{id} | 
[**existsById_0**](DefaultApi.md#existsById_0) | **GET** /api/task/exists/{id} | 
[**findAll**](DefaultApi.md#findAll) | **GET** /api/projects | 
[**findAll_0**](DefaultApi.md#findAll_0) | **GET** /api/tasks | 
[**findById**](DefaultApi.md#findById) | **GET** /api/project/{id} | 
[**findById_0**](DefaultApi.md#findById_0) | **GET** /api/task/{id} | 
[**save**](DefaultApi.md#save) | **PUT** /api/project | 
[**saveAll**](DefaultApi.md#saveAll) | **PUT** /api/projects | 
[**saveAll_0**](DefaultApi.md#saveAll_0) | **POST** /api/projects | 
[**saveAll_1**](DefaultApi.md#saveAll_1) | **PUT** /api/tasks | 
[**saveAll_2**](DefaultApi.md#saveAll_2) | **POST** /api/tasks | 
[**save_0**](DefaultApi.md#save_0) | **POST** /api/project | 
[**save_1**](DefaultApi.md#save_1) | **PUT** /api/task | 
[**save_2**](DefaultApi.md#save_2) | **POST** /api/task | 


<a name="count"></a>
# **count**
> Long count()



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    Long result = apiInstance.count();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#count");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Long**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="count_0"></a>
# **count_0**
> Long count_0()



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    Long result = apiInstance.count_0();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#count_0");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Long**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteAll"></a>
# **deleteAll**
> deleteAll()



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    apiInstance.deleteAll();
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteAll");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteAll_0"></a>
# **deleteAll_0**
> deleteAll_0(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
List<ProjectDto> body = Arrays.asList(new ProjectDto()); // List<ProjectDto> | 
try {
    apiInstance.deleteAll_0(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteAll_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;ProjectDto&gt;**](ProjectDto.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteAll_1"></a>
# **deleteAll_1**
> deleteAll_1()



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    apiInstance.deleteAll_1();
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteAll_1");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteAll_2"></a>
# **deleteAll_2**
> deleteAll_2(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
List<TaskDto> body = Arrays.asList(new TaskDto()); // List<TaskDto> | 
try {
    apiInstance.deleteAll_2(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteAll_2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;TaskDto&gt;**](TaskDto.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteById"></a>
# **deleteById**
> deleteById(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    apiInstance.deleteById(id);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteById_0"></a>
# **deleteById_0**
> deleteById_0(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    apiInstance.deleteById_0(id);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteById_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="existsById"></a>
# **existsById**
> Boolean existsById(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    Boolean result = apiInstance.existsById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#existsById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

**Boolean**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="existsById_0"></a>
# **existsById_0**
> Boolean existsById_0(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    Boolean result = apiInstance.existsById_0(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#existsById_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

**Boolean**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="findAll"></a>
# **findAll**
> List&lt;ProjectDto&gt; findAll()



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<ProjectDto> result = apiInstance.findAll();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#findAll");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;ProjectDto&gt;**](ProjectDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="findAll_0"></a>
# **findAll_0**
> List&lt;TaskDto&gt; findAll_0()



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<TaskDto> result = apiInstance.findAll_0();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#findAll_0");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;TaskDto&gt;**](TaskDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="findById"></a>
# **findById**
> ProjectDto findById(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    ProjectDto result = apiInstance.findById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#findById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

[**ProjectDto**](ProjectDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="findById_0"></a>
# **findById_0**
> TaskDto findById_0(id)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    TaskDto result = apiInstance.findById_0(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#findById_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

[**TaskDto**](TaskDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="save"></a>
# **save**
> ProjectDto save(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDto body = new ProjectDto(); // ProjectDto | 
try {
    ProjectDto result = apiInstance.save(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#save");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectDto**](ProjectDto.md)|  | [optional]

### Return type

[**ProjectDto**](ProjectDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="saveAll"></a>
# **saveAll**
> List&lt;ProjectDto&gt; saveAll(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
List<ProjectDto> body = Arrays.asList(new ProjectDto()); // List<ProjectDto> | 
try {
    List<ProjectDto> result = apiInstance.saveAll(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#saveAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;ProjectDto&gt;**](ProjectDto.md)|  | [optional]

### Return type

[**List&lt;ProjectDto&gt;**](ProjectDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="saveAll_0"></a>
# **saveAll_0**
> List&lt;ProjectDto&gt; saveAll_0(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
List<ProjectDto> body = Arrays.asList(new ProjectDto()); // List<ProjectDto> | 
try {
    List<ProjectDto> result = apiInstance.saveAll_0(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#saveAll_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;ProjectDto&gt;**](ProjectDto.md)|  | [optional]

### Return type

[**List&lt;ProjectDto&gt;**](ProjectDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="saveAll_1"></a>
# **saveAll_1**
> List&lt;TaskDto&gt; saveAll_1(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
List<TaskDto> body = Arrays.asList(new TaskDto()); // List<TaskDto> | 
try {
    List<TaskDto> result = apiInstance.saveAll_1(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#saveAll_1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;TaskDto&gt;**](TaskDto.md)|  | [optional]

### Return type

[**List&lt;TaskDto&gt;**](TaskDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="saveAll_2"></a>
# **saveAll_2**
> List&lt;TaskDto&gt; saveAll_2(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
List<TaskDto> body = Arrays.asList(new TaskDto()); // List<TaskDto> | 
try {
    List<TaskDto> result = apiInstance.saveAll_2(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#saveAll_2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;TaskDto&gt;**](TaskDto.md)|  | [optional]

### Return type

[**List&lt;TaskDto&gt;**](TaskDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="save_0"></a>
# **save_0**
> ProjectDto save_0(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDto body = new ProjectDto(); // ProjectDto | 
try {
    ProjectDto result = apiInstance.save_0(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#save_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectDto**](ProjectDto.md)|  | [optional]

### Return type

[**ProjectDto**](ProjectDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="save_1"></a>
# **save_1**
> TaskDto save_1(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDto body = new TaskDto(); // TaskDto | 
try {
    TaskDto result = apiInstance.save_1(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#save_1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TaskDto**](TaskDto.md)|  | [optional]

### Return type

[**TaskDto**](TaskDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="save_2"></a>
# **save_2**
> TaskDto save_2(body)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDto body = new TaskDto(); // TaskDto | 
try {
    TaskDto result = apiInstance.save_2(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#save_2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TaskDto**](TaskDto.md)|  | [optional]

### Return type

[**TaskDto**](TaskDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

