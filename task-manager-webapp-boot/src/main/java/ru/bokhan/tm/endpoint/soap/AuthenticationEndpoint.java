package ru.bokhan.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.dto.Result;
import ru.bokhan.tm.dto.UserDto;
import ru.bokhan.tm.repository.dto.UserDtoRepository;
import ru.bokhan.tm.util.SecurityUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService
public class AuthenticationEndpoint {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDtoRepository userDtoRepository;

    @NotNull
    @WebMethod
    public Result login(
            @NotNull @WebParam(name = "username") final String username,
            @NotNull @WebParam(name = "password") final String password
    ) {
        try {
            @NotNull final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(username, password);
            @NotNull final Authentication authentication =
                    authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (@NotNull final Exception e) {
            return new Result((e));
        }
    }

    @NotNull
    @WebMethod
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

    @Nullable
    @WebMethod
    public UserDto profile() {
        return userDtoRepository.findById(SecurityUtil.getUserId()).orElse(null);
    }

}
