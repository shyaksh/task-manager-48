package ru.bokhan.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.bokhan.tm.api.service.ITaskService;
import ru.bokhan.tm.dto.CustomUser;
import ru.bokhan.tm.dto.TaskDto;

import java.util.List;

@Controller
public class TasksController {

    @Autowired
    private ITaskService taskService;

    @GetMapping("/tasks")
    public ModelAndView index(
            @NotNull @AuthenticationPrincipal CustomUser user
    ) {
        @NotNull final List<TaskDto> tasks = taskService.findAllByUserId(user.getUserId());
        return new ModelAndView("task-list", "tasks", tasks);
    }

}